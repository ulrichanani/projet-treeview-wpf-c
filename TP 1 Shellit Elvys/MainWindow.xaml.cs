﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using System.Globalization;
using System.IO;

namespace TP_1_Shellit_Elvys
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {

            InitializeComponent();

            #region Fichier Json
            
            //*********************************************** Voici la ligne qui me pause problème ************
            string jsonString = (File.ReadAllText(@"C:\Users\Elvys\Desktop\fichier.json")).ToString();
            // J'ai mis le fichier dans le dossier du projet. C'est nommé Fichier.json
            //*************************************************************************************************

            //**************************** Tu peux aussi essayer ceci voir ************************************
            // string jsonString =@"C:\Users\Elvys\Desktop\fichier.json";
            //*************************************************************************************************


            //*********************************************** Cette ligne ne marche pas non plus **************
            // string jsonString = @"C:\Users\Elvys\Desktop\fichier.json";
            // J'ai mis le fichier dans le dossier du projet. C'est nommé Fichier.json
            //*************************************************************************************************




            //************************ Ceci marche et affiche le fichier json ****************************
            // Debug.WriteLine((File.ReadAllText(@"C:\Users\Elvys\Desktop\fichier.json")).ToString());
            // ******************************************************************************************* 

            #endregion

            //**************** Cette ligne est pour désérialiser le fichier mais je n'ai plus utilisé *******
            //string jsonString = JsonConvert.DeserializeObject<dynamic>(File.ReadAllText("Fichier.json"));
            //***********************************************************************************************

            var token = JToken.Parse(jsonString);

            var children = new List<JToken>();
            if (token != null)
            {
                children.Add(token);
            }

            treeView1.ItemsSource = null;
            treeView1.Items.Clear();
            treeView1.ItemsSource = children;

        }

        private void MenuItem_click3(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

    }
    
}
